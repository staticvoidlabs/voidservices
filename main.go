package main

import (
	Manager "gitlab.com/staticvoidlabs/voidservices/packages/managers"
	Models "gitlab.com/staticvoidlabs/voidservices/packages/models"
)

func main() {

	var mCurrentConfig Models.Config

	// Init config.
	mCurrentConfig = Manager.ProcessConfigJSON()

	// Init services.
	Manager.InitLogging()
	Manager.InitDatabase()
	go Manager.InitNappy(&mCurrentConfig)
	Manager.InitAPIService()
}
