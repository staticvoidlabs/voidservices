module gitlab.com/staticvoidlabs/voidservices

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-sqlite3 v1.14.4
	github.com/mmcdole/gofeed v1.1.0
)
