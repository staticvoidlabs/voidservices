package manager

import (
	"encoding/json"
	"fmt"
	"os"

	Models "gitlab.com/staticvoidlabs/voidservices/packages/models"
)

// Public functions.
func ProcessConfigJSON() Models.Config {

	return processConfigFile()
}

func processFeedsJSON() Models.FeedRepo {

	return processFeedsFile()
}

func GetVersion() string {

	return processConfigFile().Version
}

// Private functions.
func processConfigFile() Models.Config {

	var currentConfig Models.Config

	configFile, err := os.Open("./configs/config.json")
	defer configFile.Close()

	if err != nil {
		fmt.Println(err.Error())
	}

	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&currentConfig)

	return currentConfig
}

func processFeedsFile() Models.FeedRepo {

	var tmpFeeds Models.FeedRepo

	feedsFile, err := os.Open("./configs/feeds.json")
	defer feedsFile.Close()

	if err != nil {
		fmt.Println(err.Error())
	}

	jsonParser := json.NewDecoder(feedsFile)
	jsonParser.Decode(&tmpFeeds)

	return tmpFeeds
}
