package manager

import (
	"fmt"
	"math"
	"sort"
	"strconv"
	"time"

	"github.com/mmcdole/gofeed"
	Models "gitlab.com/staticvoidlabs/voidservices/packages/models"
)

// Member definition.
var (
	mCurrentConfig *Models.Config
	mFeedRepo      Models.FeedRepo
	//mFeedItems     []Models.FeedItem
	mFeedItemRepo   Models.FeedItemRepo
	mLastAccessedAt time.Time
	mIsFirstRun     bool
	mIsUpdating     bool
)

// InitNappy initialzes the rss service.
func InitNappy(currentConfig *Models.Config) {

	fmt.Println("Service Nappy (Version: " + currentConfig.NappyVersion + ") started.")

	mCurrentConfig = currentConfig

	mIsFirstRun = true
	mIsUpdating = false

	// Initial feed item repo update.
	updateNappy()

	// Update feed item repo periodically from now on.
	tmpIntervall, _ := strconv.Atoi(mCurrentConfig.NappyUpdateIntervall)

	ticker := time.NewTicker(time.Duration(tmpIntervall) * time.Second)
	quit := make(chan struct{}) // You can stop the worker by closing the quit channel: close(quit)

	go func() {
		for {
			select {
			case <-ticker.C:
				updateNappy()
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()

}

func updateNappy() {

	if mIsUpdating == true {
		return
	}

	mIsUpdating = true

	// Clear items repo.
	mFeedItemRepo = Models.FeedItemRepo{}

	// Import defined feed sources (feeds.json) to repo.
	mFeedRepo = processFeedsJSON()

	for _, f := range mFeedRepo.Feeds {

		fp := gofeed.NewParser()

		feed, _ := fp.ParseURL(f.Link)
		//fmt.Println("Adding: " + feed.Title)

		for _, item := range feed.Items {

			tmpNewItem := Models.FeedItem{} // {item.Title, item.Link, item.Image.URL, item.Published, item.Content}

			tmpNewItem.Origin = f.Title
			tmpNewItem.Title = item.Title
			tmpNewItem.Link = item.Link
			tmpNewItem.PublishedAt = item.Published
			tmpNewItem.IsNew = false
			tmpNewItem.Content = item.Content
			calculateItemAge(&tmpNewItem)

			if item.Image != nil {
				tmpNewItem.ImageURL = item.Image.URL
			}

			mFeedItemRepo.Items = append(mFeedItemRepo.Items, tmpNewItem)

			sort.SliceStable(mFeedItemRepo.Items, func(i, j int) bool {
				return mFeedItemRepo.Items[i].Age < mFeedItemRepo.Items[j].Age
			})

		}

	}

	// Write feed items to database.
	writeFeedItemsToDb(mFeedItemRepo.Items, true)

	tmpHelperString := "Updated "
	if mIsFirstRun {
		tmpHelperString = "Initialized "
	}

	fmt.Println(tmpHelperString + strconv.Itoa(len(mFeedRepo.Feeds)) + " feeds. Added " + strconv.Itoa(len(mFeedItemRepo.Items)) + " items.")

	mLastAccessedAt = time.Now()
	mIsFirstRun = false
	mIsUpdating = false
}

func calculateItemAge(currentItem *Models.FeedItem) {

	// 2020-11-16T17:19:43.445+01:00
	// Fri, 13 Nov 2020 18:54:00 +0100

	tmpAge := 999999
	tmpAgeLabel := "n/a"

	// Parse publishedAt time.
	tmpDateTimeClean, err := time.Parse(time.RFC3339, currentItem.PublishedAt)

	if err != nil {

		tmpDateTimeClean, err = time.Parse(time.RFC1123Z, currentItem.PublishedAt)

		if err != nil {
			fmt.Println("Error parsing date: ", err)
		}

	}

	// Calculate item age.
	tmpDateTimeNow := time.Now()

	tmpAgeHours := math.Floor(tmpDateTimeNow.Sub(tmpDateTimeClean).Hours())
	tmpAgeMinutes := math.Floor(tmpDateTimeNow.Sub(tmpDateTimeClean).Minutes())
	tmpAgeSeconds := math.Floor(tmpDateTimeNow.Sub(tmpDateTimeClean).Seconds())

	tmpAge = int(tmpDateTimeNow.Sub(tmpDateTimeClean))

	if tmpAgeHours <= 0 {
		if tmpAgeMinutes > 0 && tmpAgeHours < 60 {
			tmpAgeLabel = fmt.Sprintf("%10.0f", tmpAgeMinutes) + " Min"
		} else if tmpAgeMinutes < 0 {
			if tmpAgeSeconds > 0 && tmpAgeSeconds < 60 {
				tmpAgeLabel = fmt.Sprintf("%10.0f", tmpAgeSeconds) + " Sec"
			}
		}
	} else if tmpAgeHours > 0 && tmpAgeHours < 24 {
		tmpAgeLabel = fmt.Sprintf("%10.0f", tmpAgeHours) + " Hour"
	} else {
		tmpAgeLabel = fmt.Sprintf("%10.0f", tmpAgeHours/24) + " Day"
	}

	// tmpAge2 := tmpDateTimeNow.Sub(tmpDateTimeClean)
	// fmt.Println("Now: " + tmpDateTimeNow.String() + " Timestamp: " + tmpDateTimeClean.String() + " > Age: " + tmpAge2.String())

	currentItem.Age = tmpAge
	currentItem.AgeLabel = tmpAgeLabel
	currentItem.PublishedAtNorm = tmpDateTimeClean

}

func getUpdatedFeedItemRepo() Models.FeedItemRepo {

	tmpUpdatedmFeedItemRepo := mFeedItemRepo

	for i, item := range tmpUpdatedmFeedItemRepo.Items {

		// Set item as new/old.
		tmpDiff := int(item.PublishedAtNorm.Sub(mLastAccessedAt))

		//fmt.Println("tmpDiff: " + strconv.Itoa(tmpDiff))

		if tmpDiff > 0 || mIsFirstRun {
			tmpUpdatedmFeedItemRepo.Items[i].IsNew = true
		}
	}

	mIsFirstRun = false

	return tmpUpdatedmFeedItemRepo
}
