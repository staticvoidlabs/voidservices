package manager

import (
	"database/sql"
	"fmt"
	"strings"

	Models "gitlab.com/staticvoidlabs/voidservices/packages/models"

	_ "github.com/mattn/go-sqlite3"
)

// InitDatabase initialized the database by creating the needed tables if they not exist.
func InitDatabase() {

	db, err := sql.Open("sqlite3", "./sqlite.db")
	checkErr(err)

	statement, err := db.Prepare("CREATE TABLE IF NOT EXISTS feeditems (origin TEXT, title TEXT, link TEXT, imageURL TEXT, publishedat TEXT, content TEXT)")
	checkErr(err)
	statement.Exec()

	/*
		rows, err := db.Query("SELECT * FROM feeditems")
		checkErr(err)

		var id int
		var origin string
		var title string
		var link string

		for rows.Next() {
			rows.Scan(&id, &origin, &title, &link)
			fmt.Println(strconv.Itoa(id) + ": " + origin + " " + title + " " + link)
		}
	*/

	db.Close()
}

func writeFeedItemsToDb(feedItems []Models.FeedItem, purgeDb bool) {

	db, err := sql.Open("sqlite3", "./sqlite.db")
	checkErr(err)

	for _, fi := range feedItems {

		// Remove problematic characers in strings.
		fi.Title = strings.Replace(fi.Title, "\"", "", -1)
		fi.Content = strings.Replace(fi.Content, "\"", "", -1)

		// Truncate table.
		tmpStatement, err := db.Prepare("DELETE FROM feeditems")
		checkErr(err)

		tmpStatement.Exec()

		// Prepare sql statement for current item.
		tmpStatement, err = db.Prepare("INSERT INTO feeditems (origin, title, link, imageURL, publishedat, content) VALUES (\"" + fi.Origin + "\", \"" + fi.Title + "\", \"" + fi.Link + "\", \"" + fi.ImageURL + "\", \"" + fi.PublishedAt + "\", \"" + fi.Content + "\")")
		checkErr(err)

		tmpStatement.Exec()

	}

	db.Close()
}

func checkErr(err error) {

	if err != nil {
		fmt.Println("Error: ", err)
	}

}
