package manager

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// InitAPIService starts the API service.
func InitAPIService() {

	WriteLogMessage("Starting Web API services", true)

	// Create router instance.
	router := mux.NewRouter().StrictSlash(true)

	// Define routes and actions.
	router.HandleFunc("/", getStateInfoService)
	router.HandleFunc("/nappy", getStateInfoNappy)
	router.HandleFunc("/nappy/update", updateNappyNews)
	router.HandleFunc("/nappy/news/raw", getNappyNews)
	router.HandleFunc("/nappy/ctrl/restart", restartNappy)
	router.HandleFunc("/nappy/news", generateHTMLOutput)
	//router.PathPrefix("/nappy/news").Handler(http.StripPrefix("/nappy/news", http.FileServer(http.Dir("./www"))))

	// Start rest service.
	go log.Fatal(http.ListenAndServe(":9100", router))

}

// Endpoint implementation.
func getStateInfoService(w http.ResponseWriter, r *http.Request) {

	tmpLogMessages := GetCurrentLog()

	for _, message := range tmpLogMessages {
		w.Write([]byte(message + "\r\n"))
	}

}

func updateNappyNews(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:updateNappyNews", true)

	updateNappy()

	json.NewEncoder(w).Encode("VoidServices_response:update_nappy_news:success.")

}

func getStateInfoNappy(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:getStateInfoNappy", true)

	json.NewEncoder(w).Encode("VoidServices_response:get_nappy_state_info:success.")
}

func getNappyNews(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:getNappyNews", true)

	//json.NewEncoder(w).Encode("VoidServices_response:get_nappy_news_items:success.")
	json.NewEncoder(w).Encode(mFeedItemRepo.Items)
}

func restartNappy(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:restartNappy", true)

	json.NewEncoder(w).Encode("VoidServices_response:restart_nappy:success.")
}

func generateHTMLOutput(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:generateHTMLOutput", true)

	// Try to parse template.
	tmpl, err := template.ParseFiles("./www/template_news_simple.html")
	if err != nil {
		fmt.Println("Error parsing template: ", err)
	}

	mFeedItemRepo = getUpdatedFeedItemRepo()

	err = tmpl.Execute(w, mFeedItemRepo)
	if err != nil {
		fmt.Println("Error executing template: ", err)
	}

	//json.NewEncoder(w).Encode("VoidServices_response:generate_HTML_Output:success.")
}

func htmlTesting(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:restartNappy", true)

	//json.NewEncoder(w).Encode("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
	fmt.Print("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
}
