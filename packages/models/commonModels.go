package model

import "time"

// Section Config models.
type Config struct {
	Version              string `json:"version"`
	NappyVersion         string `json:"nappyVersion"`
	NappyUpdateIntervall string `json:"nappyUpdateIntervall"`
}

// Section Feed models.
type FeedRepo struct {
	Feeds []Feed
}

type Feed struct {
	Title    string `json:"title"`
	Link     string `json:"link"`
	FeedIcon string `json:"feedIcon"`
	IsActive bool   `json:"isActive"`
}

type FeedItemRepo struct {
	Items []FeedItem
}

type FeedItem struct {
	Origin          string
	Title           string
	Link            string
	ImageURL        string
	PublishedAt     string
	PublishedAtNorm time.Time
	Age             int
	AgeLabel        string
	Content         string
	IsNew           bool
}
